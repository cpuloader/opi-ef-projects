FILE=./project.txt
DEFAULT_PROJECT=dubgen

OUT_SETTINGS_FILE=./out-params.txt
DEFAULT_OUT_DEVICE=0

if [ -f $FILE ]; then
  echo "EF_START_SCRIPT: File $FILE exists."
  PROJECT=`cat $FILE`
  echo "EF_START_SCRIPT: project file data is $PROJECT"
  if [ -z "$PROJECT" ]; then
    echo "EF_START_SCRIPT: Config is empty"
    PROJECT=$DEFAULT_PROJECT
  fi
else
   echo "EF_START_SCRIPT: File $FILE does not exist."
   PROJECT=$DEFAULT_PROJECT
fi

if [ -f $OUT_SETTINGS_FILE ]; then
  echo "EF_START_SCRIPT: File $OUT_SETTINGS_FILE exists."
  OUT_DEVICE=`cat $OUT_SETTINGS_FILE`
  echo "EF_START_SCRIPT: output device is $OUT_DEVICE"
  if [ -z "$OUT_DEVICE" ]; then
    echo "EF_START_SCRIPT: Output device config is empty"
    OUT_DEVICE=$DEFAULT_OUT_DEVICE
  fi
else
   echo "EF_START_SCRIPT: File $OUT_SETTINGS_FILE does not exist."
   OUT_DEVICE=$DEFAULT_OUT_DEVICE
fi

echo "EF_START_SCRIPT: Will play $PROJECT project with output device $OUT_DEVICE"
cd $PROJECT
su -c "./play-release -s -c$OUT_DEVICE" root &
